package imageupload.aj.com.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity  {
    private EditText mNameInput;
    private EditText mLocationInput;
    private EditText mAboutInput;
    private EditText mContact;

    private ImageView mAvatarImage;
    private ImageView mCoverImage;
    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    Button pick,upload;
    File file;
    byte[] bytes;
    String fileName;
    String extension;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        pick = (Button)findViewById(R.id.pick);
        upload = (Button)findViewById(R.id.upload);
        pd = new ProgressDialog(MainActivity.this);
        pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("*/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveProfileAccount();
            }
        });

        // do anything before post data.. or triggered after button clicked
       // saveProfileAccount();
    }

    private void saveProfileAccount() {
        // loading or check internet connection or something...
        // ... then
        pd.setMessage("Loading");
        pd.show();
        String url = "http://192.168.1.99/fileupload/php/ad.php";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                try {
                    Toast.makeText(getApplicationContext(),""+response.toString(),Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                pd.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.cancel();
                Toast.makeText(getApplicationContext(),error.getCause()+" \n"+error.getMessage(),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("uname", "razik");
//                params.put("name", mNameInput.getText().toString());
//                params.put("location", mLocationInput.getText().toString());
//                params.put("about", mAvatarInput.getText().toString());
//                params.put("contact", mContactInput.getText().toString());
                return params;
            }



            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
//                params.put("avatar", new DataPart("file_avatar.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mAvatarImage.getDrawable()), "image/jpeg"));
                params.put("file", new DataPart(fileName+""+extension,bytes, "*/*"));

                return params;
            }
        };
       // multipartRequest.setOnProgressListener(this);
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);

    }
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();


            try {
             File file = new File(SelectedFilePath.getPath(getApplicationContext(),filePath));
                fileName = file.getName();
                extension = fileName.substring(fileName.lastIndexOf("."));
                bytes = loadFile(file);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),"ERROR "+e.getMessage()+"\n"+e.getCause(),Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

         /*   try {
                //Getting the Bitmap from Gallery
              //  bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                //Setting the Bitmap to ImageView
             //   imageView.setImageBitmap(bitmap);
            } catch (IOException e) {

                e.printStackTrace();
            } */
        }
    }
    private void saveProfileAccount2() {
        // loading or check internet connection or something...
        // ... then
        String url = "http://192.168.1.99/fileupload/php/ad.php";

    }

}